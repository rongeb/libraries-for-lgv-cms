<?php
namespace ExtLib;

class MCryptConst {
    public static $METHODAES256 = 'aes-256-cbc';
    public static $iv = 'fedcba9876543210'; //Same as in JAVA
    public static $key = 'a012345678901234567890123456789z'; //Same as in JAVA 
}
